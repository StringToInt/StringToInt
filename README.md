### ✋ About myself

- Co-founder of Sunhat
- Previously Senior Software Engineer at [LeanIX](https://www.leanix.net/en) & Co-founder of [Sedeo](https://sedeo.net)
- I like to work with Angular, Node.js, TypeScript and Vue.js
- Passion for software engineering, SaaS, startups, test-driven development, creating fancy UIs and more
- Looking forward to build great things

### 🚀 Experience

- 🇩🇪 ⭐️ Development of a Software-as-a-Service platform for Enterprise Architecture (EA), enabling organizations to make faster, data-driven decisions at [LeanIX](https://www.leanix.net/en) in Bonn. Tech stack: Angular, TypeScript, Java, Node.js, Bootstrap, Protractor
- 🇩🇪 ⭐️ Development of a Software-as-a-Service application to manage and book workspaces and meetingrooms to enable companies to use desk sharing at [Sedeo](https://sedeo.net) in Cologne. Tech stack: Angular, TypeScript, Java, Node.js, MongoDB, Bulma, Protractor
- 🇩🇪 Development of a B2B software in the area of app pre-installation at [freenet Group](https://www.freenet-group.de/en/index.html) (remote). Tech stack: Java, Android
- 🇩🇪 [Sportwetten news](https://www.sportwetten.de/news): Development of a news portal on which articles and analyses about current sporting events are regularly published for a customer. Tech stack: WordPress, PHP, JavaScript, Bootstrap
- 🇩🇪 Development of an intelligent digitisation platform that allows companies to immediately benefit from the advantages of digital technologies at an IT company in Cologne. Tech stack: Angular, TypeScript, Bootstrap

### 💻 Open Source

- [es-check-action](https://github.com/marketplace/actions/es-check-action): a GitHub action which checks JavaScript files against a specified version of ECMAScript (ES). If a specified file's ES version doesn't match the ES version argument, this action will throw an error.
- [text-select](https://github.com/ali-kamalizade/text-select): a tiny and helpful JavaScript library to easily select text on web pages. Available on NPM.
- [better-protractor](https://github.com/ali-kamalizade/better-protractor): write better end-to-end tests by providing a simple API on top of Protractor. Available on NPM.
- [scripts](https://github.com/ali-kamalizade/scripts): a collection of reusable scripts, functions and aliases related to development 
- 🚧

### 🖤 Getting things done with

- [GitHub](https://github.com/)
- [GitLab](https://about.gitlab.com) (obviously)
- [MongoDB](https://www.mongodb.com/cloud/atlas)
- [Netlify](https://www.netlify.com)
- [Render](https://render.com)

### 🌏 Let's connect

- [Connect with me on LinkedIn](https://www.linkedin.com/in/alikamalizade)
- [Follow me on Twitter](https://twitter.com/AliDev94)
- [Check out my blog](https://ali-dev.medium.com)

### 🍏 Everything else

- 🤝 Officially recognized friend of TypeScript
- 🆘 Reported one security vulnerability for a popular open-source project and got paid for it
- 🇯🇵 有難うございます。
